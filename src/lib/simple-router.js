export function createRouter () {
	let routes = [];
	let outlet;

	return {
		/**
		 * @param {HashChangeEvent} event
		 */
		handleEvent (event) {
			if (event.type === 'hashchange') {
				this.run(location.hash.slice(1));
			}
		},

		/**
		 * @param {string} pathname
		 * @param {(params: Record<string, string>, path: Path) => void} handler
		 * @param {boolean} [end]
		 */
		on (pathname, handler, end = true) {
			let matcher = compilePath(pathname, end);
			routes.push({ matcher, handler });
		},

		/**
		 * @param {string} uri
		 */
		run (uri) {
			let path = parsePath(uri);
			for (let i = 0, l = routes.length; i < l; i++) {
				let route = routes[i];
				let match = route.matcher.exec(path.pathname);

				if (match) {
					route.handler(match.groups, path);
					return;
				}
			}
		},

		/**
		 * @param {HTMLElement} target
		 */
		listen (target) {
			if (!outlet) {
				outlet = target;

				this.run(location.hash.slice(1));
				window.addEventListener('hashchange', this);
			}
		},

		stop () {
			window.removeEventListener('hashchange', this);
		},
	};
}

/**
 * @typedef {object} Path
 * @property {string} pathname
 * @property {string} [hash]
 * @property {string} [search]
 */

/**
 * @param {string} path
 * @returns
 */
function parsePath (path) {
	let parsed = { pathname: '/' };

	if (path) {
		let hashIndex = path.indexOf('#');
		if (hashIndex >= 0) {
			parsed.hash = path.slice(hashIndex);
			path = path.slice(0, hashIndex);
		}

		let searchIndex = path.indexOf('?');
		if (searchIndex >= 0) {
			parsed.search = path.slice(searchIndex);
			path = path.slice(0, searchIndex);
		}

		if (path) {
			parsed.pathname = path;
		}
	}

	return parsed;
}

/**
 * @param {string} path
 * @param {boolean} [end]
 * @param {boolean} [caseSensitive]
 * @returns {RegExp}
 */
function compilePath (path, end = false, caseSensitive = false) {
	let source = path
		.replace(/^\/*/, '/')
		.replace(/\/?\*?$/, '')
		.replace(/[\\.*+^$?{}|()[\]]/g, '\\$&')
		.replace(/:(\w+)/g, '(?<$1>[^\/]+)');

	let re = '^(' + source + ')';
	let flags = caseSensitive ? undefined : 'i';

	if (path.at(-1) === '*') {
		if (path.at(-2) === '/') {
			re += '(?:\\/(?<$>.+)|\\/?)';
    } else {
      re += '(?<$>.*)';
    }
	}
	else if (end) {
		re += '\\/?$';
	}

	return new RegExp(re, flags);
}
