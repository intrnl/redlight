import { signal, computed, effect, batch } from '@intrnl/velvet';
import { is_function, noop } from '@intrnl/velvet/internal';


export const globalCache = new Map();

function createCacheItem (cache, key) {
	const stringified = JSON.stringify(Array.isArray(key) ? key : [key]);
	let item = cache.get(stringified);

	if (!item) {
		item = {
			data: signal(null),
			error: signal(null),
			lastUpdated: signal(0),

			request: signal(null),
			stale: signal(true),
		};

		cache.set(stringified, item);
	}

	return item;
}

async function refreshCache (store, data, force = true) {
	if (data === undefined) {
		store.stale.value = true;
		return;
	}

	if (!force) {
		const prevRequest = store.request.peek();

		if (prevRequest) {
			return prevRequest;
		}
	}

	let request;
	if (is_function(data)) {
		request = Promise.resolve(data(store.data.peek()));
	}
	else {
		request = Promise.resolve(data);
	}

	store.request.value = request;

	let hasError = true;
	let result = null;
	let error = null;

	try {
		result = await request;
	}
	catch (err) {
		hasError = true;
		error = err;
	}

	if (store.request.peek() === request) {
		batch(() => {
			store.data.value = result;
			store.error.value = error;
			store.lastUpdated.value = Date.now();

			store.stale.value = false;
			store.request.value = null;
		});
	}

	if (hasError) {
		throw error;
	}

	return result;
}

export async function defaultFetcher (key) {
	const response = await fetch(key);

	if (!response.ok) {
		throw new Error(`Response error ${response.status}`);
	}

	const json = await response.json();
	return json;
}

export function query (key, options = {}) {
	const {
		fetcher = defaultFetcher,
		cache = globalCache,
		plugins = [],
	} = options;

	const toggle = signal(false);
	let _store;

	let _refresh;

	let destroy = noop;

	effect(() => {
		destroy();

		const nextKey = typeof key === 'function' ? key() : key;
		const nextStore = createCacheItem(cache, nextKey);
		const updater = () => fetcher(key);

		if (_store !== nextStore) {
			toggle.value = !toggle.value;
			_store = nextStore;

			_refresh = (force) => refreshCache(nextStore, updater, force);
		}

		if (maxAge != null && Date.now() - maxAge > nextStore.lastUpdated.peek()) {
			_refresh(false);
		}

		const dispose = effect(() => {
			if (nextStore.stale.value) {
				_refresh(false);
			}
		});

		const subscriptions = plugins.map((fn) => {
			const result = fn({
				key: nextKey,
				data: nextStore.data,
				error: nextStore.error,
				lastUpdated: nextStore.lastUpdated,
				refresh: _refresh,
			});

			return result || noop;
		});

		cleanup = () => {
			dispose();
			subscriptions.forEach((fn) => fn());

			cleanup = noop;
		};
	});

	cleanup(() => destroy());

	return {
		data: computed(() => (toggle.value, _store.data.value)),
		error: computed(() => (toggle.value, _store.error.value)),
		processing: computed(() => (toggle.value, !!_store.request.value)),
		refresh: () => _refresh(true),
		mutate: (next) => refreshCache(_store, next, true),
	};
}
