export const request = async (uri) => {
	const response = await fetch(`https://www.reddit.com${uri}`);

	if (!response.ok) {
		throw new Error(`Response error ${response.status}`);
	}

	const json = await response.json();
	return json;
};

export const decodeHtml = (html) => {
	if (!html) {
		return html;
	}

	const elem = document.createElement('textarea');
	elem.innerHTML = html;
	return elem.value;
};

export const fixVar = (value, array) => {
	if (!array.includes(value)) {
		return array[0];
	}

	return value;
};
