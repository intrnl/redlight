const colors = [
	'#EA0027', '#FF4500', '#FF8717', '#FFD635', '#DDBD37', '#94E044', '#0CB52B',
	'#0DD3BB', '#00A6A5', '#24A0ED', '#0079D3', '#7193FF'
];

const links = Array.from({ length: 51 }, (_, idx) => (
	`https://www.redditstatic.com/mweb2x/img/snoovatars/snoovatar_${idx + 1}.png`
));

export function getSnoovatar (name) {
	let seed = 0;
	let lowercase = name.toLowerCase();

	for (let i = 0, l = lowercase.length; i < l; i++) {
		seed += lowercase.charCodeAt(i);
	}

	return {
		color: colors[seed % colors.length],
		src: links[seed % links.length],
	}
}
