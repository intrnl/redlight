import { batch } from '@intrnl/velvet';
import { createRouter } from '~/lib/simple-router.js';

import '~/styles/app.css';

import '~/views/EmptyView.velvet';
import '~/views/ListingView.velvet';
import '~/views/PostView.velvet';

let initial = true;

const container = document.querySelector('div.container');

const listingView = document.querySelector('x-listing-view');
const postView = document.querySelector('x-post-view');

const router = createRouter();

router.on('/', (params, path) => {
	const searchParams = new URLSearchParams(path.search);
	container.className = 'container view-listing';

	batch(() => {
		listingView.subredditId = null;
		listingView.sortBy = searchParams.get('sort');
		listingView.periodBy = searchParams.get('period');
	});
});

router.on('/r/:subreddit', (params, path) => {
	const searchParams = new URLSearchParams(path.search);
	container.className = 'container view-listing';

	batch(() => {
		listingView.subredditId = params.subreddit;
		listingView.sortBy = searchParams.get('sort');
		listingView.periodBy = searchParams.get('period');
	});
});

router.on('/r/:subreddit/comments/:post', (params, path) => {
	container.className = 'container view-post';

	batch(() => {
		postView.subredditId = params.subreddit;
		postView.postId = params.post;
		postView.commentId = null;

		if (initial) {
			listingView.subredditId = params.subreddit;
		}
	});
});

router.on('/r/:subreddit/comments/:post/comment/:comment', (params, path) => {
	container.className = 'container view-post';

	batch(() => {
		postView.subredditId = params.subreddit;
		postView.postId = params.post;
		postView.commentId = params.comment;

		if (initial) {
			listingView.subredditId = params.subreddit;
		}
	});
});

router.listen();
initial = false;
